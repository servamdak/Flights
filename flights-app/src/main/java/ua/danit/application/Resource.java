package ua.danit.application;

import javax.ws.rs.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Resource {
  public static final String HTML_TEMPLATE = "<!DOCTYPE html>\n"
      + "<html>\n<head><title>British Airlines</title></head>"
      + "<body><h1 style=\" text-align: center; \">%s</h1></body></html>";

  @Path("/weather")
  @GET
  public String getWeather() {
    return sendResponse("The weather is cool today. Isn't it?");
  }

  @GET
  @Path("/flights")
  public String getFlights() {
    return sendResponse("Today all routes closed");
  }

  @Path("/tickets")
  @POST
  public String getTickets() {
    return sendResponse("No ticket available");
  }

  /**
   * This method (getUsers)returns
   * String to sendPesponse.
   */
  @Path("/query")
  @GET
  public String getUsers(@QueryParam("user") String name) {
    return sendResponse("User name is : " + name);

  }

  private String sendResponse(String response) {
    return String.format(HTML_TEMPLATE, response);
  }
}
