package ua.danit.application.resources;

import org.codehaus.jackson.map.ObjectMapper;
import ua.danit.application.dao.FlightDao;
import ua.danit.application.model.Flight;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.io.IOException;

public class FlightResource {

  private FlightDao flightDao;

//  public FlightResource(FlightDao flightDao) {
//    this.flightDao = flightDao;
//  }

  @Path("/flights/top")
  @GET
  public String getTopFlights() throws IOException {
    if (flightDao == null) {
      flightDao = new FlightDao();
    }

    Iterable<Flight> flights = flightDao.getTopFlight(5);
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(flights);
  }

  //TODO: add new Path for Get By Id
}
