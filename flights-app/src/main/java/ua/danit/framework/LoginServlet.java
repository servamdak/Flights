package ua.danit.framework;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

  private static final String loginForm =
      "<html><body>" +
      " <form method = 'post'>" +
      " <input name = 'login' type = 'text'><br>" +
      " <input name = 'password' type = 'password'><br>" +
      " <button type = 'submit'>Submit</button><br>" +
      "</form></body></html>";

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String loginName = null;

    if (req.getCookies() != null) {

      for (Cookie cookie : req.getCookies()) {
        String cookieName = cookie.getName();
        if (cookieName != null && cookieName.equals("flight-user") && cookie.getMaxAge() == -1) {
          loginName = cookie.getValue();
        }
      }
    }

    if (loginName != null && !loginName.isEmpty()) {
      resp.sendRedirect("/search");
    } else {
      resp.getWriter().write(Files.toString(new File("flights-app/resources/login.html"), Charsets.UTF_8));
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String login = req.getParameter("login");
    String password = req.getParameter("password");
    Cookie cookie = new Cookie("flight-user", login);
    cookie.setMaxAge(-1);

    resp.addCookie(cookie);

    resp.sendRedirect("/search");
  }
}
